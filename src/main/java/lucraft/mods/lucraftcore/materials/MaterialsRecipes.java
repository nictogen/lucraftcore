package lucraft.mods.lucraftcore.materials;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.materials.Material.MaterialComponent;
import lucraft.mods.lucraftcore.util.helper.mods.ThermalExpansionHelper;
import lucraft.mods.lucraftcore.utilities.blocks.UtilitiesBlocks;
import lucraft.mods.lucraftcore.utilities.items.UtilitiesItems;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.oredict.OreDictionary;

public class MaterialsRecipes {

	public static boolean generateRecipeJson = false;

	@SubscribeEvent
	public void onRegisterRecipes(RegistryEvent.Register<IRecipe> e) {
		if (generateRecipeJson) {

			for (Material m : Material.getMaterials()) {
				// Ingot -> Block
				if (m.autoGenerateComponent(MaterialComponent.BLOCK))
					addShapedRecipe(m.getItemStack(MaterialComponent.BLOCK), new ResourceLocation(LucraftCore.MODID, "ingots_to_block"), "XXX", "XXX", "XXX", 'X', m.getOreDictionaryName(MaterialComponent.INGOT));

				// Block -> Ingot
				if (m.autoGenerateComponent(MaterialComponent.INGOT))
					addShapedRecipe(m.getItemStack(MaterialComponent.INGOT, 9), new ResourceLocation(LucraftCore.MODID, "block_to_ingots"), "X", 'X', m.getOreDictionaryName(MaterialComponent.BLOCK));

				// Nugget -> Ingot
				if (m.autoGenerateComponent(MaterialComponent.INGOT))
					addShapedRecipe(m.getItemStack(MaterialComponent.INGOT), new ResourceLocation(LucraftCore.MODID, "nuggets_to_ingot"), "XXX", "XXX", "XXX", 'X', m.getOreDictionaryName(MaterialComponent.NUGGET));

				// Ingot -> Nugget
				if (m.autoGenerateComponent(MaterialComponent.NUGGET))
					addShapedRecipe(m.getItemStack(MaterialComponent.NUGGET, 9), new ResourceLocation(LucraftCore.MODID, "ingot_to_nuggets"), "X", 'X', m.getOreDictionaryName(MaterialComponent.INGOT));

				// Ingot -> Plate
				if (m.autoGenerateComponent(MaterialComponent.PLATE))
					addShapelessRecipe(m.getItemStack(MaterialComponent.PLATE), new ResourceLocation(LucraftCore.MODID, "ingots_to_plate"), new ItemStack(UtilitiesItems.HAMMER, 1, OreDictionary.WILDCARD_VALUE), m.getItemStack(MaterialComponent.INGOT), m.getItemStack(MaterialComponent.INGOT));

				// Ingot -> Dust
				if (m.autoGenerateComponent(MaterialComponent.DUST))
					addShapelessRecipe(m.getItemStack(MaterialComponent.DUST), new ResourceLocation(LucraftCore.MODID, "ingot_to_dust"), new ItemStack(UtilitiesItems.HAMMER, 1, OreDictionary.WILDCARD_VALUE), m.getItemStack(MaterialComponent.INGOT));
			}

			for (EnumDyeColor color : EnumDyeColor.values()) {
				String dye = "dye" + color.getUnlocalizedName().substring(0, 1).toUpperCase() + color.getUnlocalizedName().substring(1, color.getUnlocalizedName().length());
				if(color == EnumDyeColor.SILVER)
					dye = "dyeLightGray";
				addShapelessRecipe(new ItemStack(UtilitiesItems.TRI_POLYMER.get(color)), new ResourceLocation(LucraftCore.MODID, "tri_polymer"), dye, "plateIron", Items.LEATHER);
			}

			addShapedRecipe(new ItemStack(UtilitiesItems.HAMMER), " XX", "SSX", " XX", 'X', "ingotIron", 'S', "stickWood");
			addShapedRecipe(new ItemStack(UtilitiesItems.INJECTION), "  N", "IP ", "PI ", 'P', "plateTin", 'N', "nuggetSilver", 'I', "ingotIron");
			addShapedRecipe(new ItemStack(UtilitiesItems.LV_CAPACITOR), new ResourceLocation(LucraftCore.MODID, "capacitor"), "IRI", "PLP", "PCP", 'R', "dustRedstone", 'I', "ingotIron", 'P', "plateIron", 'L', "ingotLead", 'C', "plateCopper");
			addShapedRecipe(new ItemStack(UtilitiesItems.MV_CAPACITOR), new ResourceLocation(LucraftCore.MODID, "capacitor"), "IRI", "PLP", "PCP", 'R', "blockRedstone", 'I', "ingotIron", 'P', "plateIron", 'L', "ingotLead", 'C', "plateOsmium");
			addShapedRecipe(new ItemStack(UtilitiesItems.HV_CAPACITOR), new ResourceLocation(LucraftCore.MODID, "capacitor"), "IRI", "PLP", "PCP", 'R', "blockRedstone", 'I', "ingotSteel", 'P', "plateIron", 'L', "blockLead", 'C', "plateIntertium");
			addShapedRecipe(new ItemStack(UtilitiesItems.WIRE_CUTTER), " P ", "S S", 'P', "plateIron", 'S', "stickWood");
			addShapelessRecipe(Material.COPPER.getItemStack(MaterialComponent.WIRING, 4), "plateCopper", UtilitiesItems.WIRE_CUTTER);
			addShapedRecipe(new ItemStack(UtilitiesItems.SERVO_MOTOR), " P ", "WIW", " P ", 'P', "plateIron", 'I', "ingotIron", 'W', "wiringCopper");
			addShapedRecipe(new ItemStack(UtilitiesItems.BASIC_CIRCUIT), new ResourceLocation(LucraftCore.MODID, "circuit"), "WWW", "RPR", "WWW", 'W', "wiringCopper", 'R', "dustRedstone", 'P', "plateIron");
			addShapedRecipe(new ItemStack(UtilitiesItems.ADVANCED_CIRCUIT), new ResourceLocation(LucraftCore.MODID, "circuit"), "WCW", "RPR", "WCW", 'W', "wiringCopper", 'R', "dustRedstone", 'P', UtilitiesItems.BASIC_CIRCUIT, 'C', UtilitiesItems.LV_CAPACITOR);
			addShapedRecipe(new ItemStack(UtilitiesBlocks.CONSTRUCTION_TABLE), "PPP", "ICI", "IPI", 'P', "plateIron", 'C', Blocks.CRAFTING_TABLE, 'I', "ingotIron");

			addShapelessRecipe(Material.ADAMANTIUM.getItemStack(MaterialComponent.DUST, 3), new ResourceLocation(LucraftCore.MODID, "alloys"), "dustSteel", "dustSteel", "dustVibranium");
			addShapelessRecipe(Material.INTERTIUM.getItemStack(MaterialComponent.DUST, 3), new ResourceLocation(LucraftCore.MODID, "alloys"), "dustIron", "dustIron", "dustOsmium");
			addShapelessRecipe(Material.BRONZE.getItemStack(MaterialComponent.DUST, 2), new ResourceLocation(LucraftCore.MODID, "alloys"), "dustCopper", "dustCopper", "dustCopper", "dustTin");
			addShapelessRecipe(Material.GOLD_TITANIUM_ALLOY.getItemStack(MaterialComponent.DUST, 3), new ResourceLocation(LucraftCore.MODID, "alloys"), "dustGold", "dustTitanium", "dustTitanium");

			generateConstants();
		}
	}

	public static void init() {
		// OreDictionary Registry
		for (Material m : Material.getMaterials()) {
			for (MaterialComponent mc : MaterialComponent.values()) {
				if (mc != MaterialComponent.ALL && mc != MaterialComponent.FLUID && m.autoGenerateComponent(mc)) {
					OreDictionary.registerOre(m.getOreDictionaryName(mc), m.getItemStack(mc));
				}
			}
		}
	}

	public static void postInit() {
		for (String name : OreDictionary.getOreNames()) {
			if (isExistingOreName(name)) {

				// Ores
				if (name.startsWith("ore")) {
					String ore = name.substring("ore".length());

					for (ItemStack ores : OreDictionary.getOres("ore" + ore)) {
						for (ItemStack dusts : OreDictionary.getOres("dust" + ore)) {
							ItemStack output = dusts.copy();
							output.setCount(2);
						}

						for (ItemStack ingots : OreDictionary.getOres("ingot" + ore)) {
							GameRegistry.addSmelting(ores, ingots, 1F);
						}
					}
				}

				// Ingots
				if (name.startsWith("ingot")) {
					String ore = name.substring("ingot".length());

					for (ItemStack ingots : OreDictionary.getOres("ingot" + ore)) {
						for (ItemStack dusts : OreDictionary.getOres("dust" + ore)) {
							if (!ore.equalsIgnoreCase("steel"))
								GameRegistry.addSmelting(dusts, ingots, 1F);
						}

						for (ItemStack plates : OreDictionary.getOres("plate" + ore)) {
						}
					}
				}
			}
		}

		// Bronze
		addAlloyRecipe("ingotTin", 1, "ingotCopper", 3, "ingotBronze", 4);
		addAlloyRecipe("ingotTin", 1, "dustCopper", 3, "ingotBronze", 4);
		addAlloyRecipe("dustTin", 1, "ingotCopper", 3, "ingotBronze", 4);
		addAlloyRecipe("dustTin", 1, "dustCopper", 3, "ingotBronze", 4);

		// Adamantium
		addAlloyRecipe("ingotSteel", 2, "ingotVibranium", 1, "ingotAdamantium", 3);
		addAlloyRecipe("ingotSteel", 2, "dustVibranium", 1, "ingotAdamantium", 3);
		addAlloyRecipe("dustSteel", 2, "ingotVibranium", 1, "ingotAdamantium", 3);
		addAlloyRecipe("dustSteel", 2, "dustVibranium", 1, "ingotAdamantium", 3);

		// Intertium
		addAlloyRecipe("ingotIron", 2, "ingotOsmium", 1, "ingotIntertium", 3);
		addAlloyRecipe("ingotIron", 2, "dustOsmium", 1, "ingotIntertium", 3);
		addAlloyRecipe("dustIron", 2, "ingotOsmium", 1, "ingotIntertium", 3);
		addAlloyRecipe("dustIron", 2, "dustOsmium", 1, "ingotIntertium", 3);

		// Gold-Titanium-Alloy
		addAlloyRecipe("ingotGold", 1, "ingotTitanium", 2, "ingotGoldTitaniumAlloy", 3);
		addAlloyRecipe("ingotGold", 1, "dustGold", 2, "ingotGoldTitaniumAlloy", 3);
		addAlloyRecipe("dustGold", 1, "ingotTitanium", 2, "ingotGoldTitaniumAlloy", 3);
		addAlloyRecipe("dustGold", 1, "dustTitanium", 2, "ingotGoldTitaniumAlloy", 3);

		// Invar
		addAlloyRecipe("ingotIron", 2, "ingotNickel", 1, "ingotInvar", 3);
		addAlloyRecipe("ingotIron", 2, "dustIron", 1, "ingotInvar", 3);
		addAlloyRecipe("dustIron", 2, "ingotNickel", 1, "ingotInvar", 3);
		addAlloyRecipe("dustIron", 2, "dustNickel", 1, "ingotInvar", 3);

		// Electrum
		addAlloyRecipe("ingotGold", 1, "ingotSilver", 1, "ingotElectrum", 2);
		addAlloyRecipe("ingotGold", 1, "dustGold", 1, "ingotElectrum", 2);
		addAlloyRecipe("dustGold", 1, "ingotSilver", 1, "ingotElectrum", 2);
		addAlloyRecipe("dustGold", 2, "dustSilver", 1, "ingotElectrum", 3);

		// Steel
		addAlloyRecipe("dustIron", 1, "dustCoal", 2, "ingotSteel", 1);
		addAlloyRecipe("dustIron", 1, "dustCharcoal", 4, "ingotSteel", 1);
		addAlloyRecipe("ingotIron", 1, "dustCoal", 2, "ingotSteel", 1);
		addAlloyRecipe("ingotIron", 1, "dustCharcoal", 4, "ingotSteel", 1);
		addAlloyRecipe("dustSteel", 1, "dustCoal", 2, "ingotSteel", 1);
		addAlloyRecipe("dustSteel", 1, "dustCharcoal", 2, "ingotSteel", 1);
	}

	public static boolean isExistingOreName(String name) {
		if (!OreDictionary.doesOreNameExist(name))
			return false;
		else
			return !OreDictionary.getOres(name).isEmpty();
	}

	public static void addAlloyRecipe(String ore1, int amount1, String ore2, int amount2, String output, int amountOutput) {
		for (ItemStack i1 : OreDictionary.getOres(ore1)) {
			for (ItemStack i2 : OreDictionary.getOres(ore2)) {
				for (ItemStack i3 : OreDictionary.getOres(output)) {
					ItemStack ingredient1 = i1.copy();
					ingredient1.setCount(amount1);
					ItemStack ingredient2 = i2.copy();
					ingredient2.setCount(amount2);
					ItemStack outputStack = i3.copy();
					outputStack.setCount(amountOutput);

					// addAlloySmelterRecipe(ingredient1, ingredient2,
					// outputStack, 2F);
					int amount = ingredient1.getCount() + ingredient2.getCount();
					ThermalExpansionHelper.addSmelterRecipe(amount * 1200, ingredient1, ingredient2, outputStack);
				}
			}
		}
	}

	// Credit:
	// https://gist.github.com/williewillus/a1a899ce5b0f0ba099078d46ae3dae6e

	private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();
	private static File RECIPE_DIR = null;
	private static final Set<String> USED_OD_NAMES = new TreeSet<>();

	private static void setupDir() {
		if (RECIPE_DIR == null) {
			RECIPE_DIR = new File("recipes");
		}

		if (!RECIPE_DIR.exists()) {
			RECIPE_DIR.mkdir();
		}
	}

	private static void addShapedRecipe(ItemStack result, Object... components) {
		addShapedRecipe(result, null, components);
	}

	private static void addShapedRecipe(ItemStack result, ResourceLocation group, Object... components) {
		setupDir();

		// GameRegistry.addShapedRecipe(result, components);

		Map<String, Object> json = new HashMap<>();

		List<String> pattern = new ArrayList<>();
		int i = 0;
		while (i < components.length && components[i] instanceof String) {
			pattern.add((String) components[i]);
			i++;
		}
		json.put("pattern", pattern);

		boolean isOreDict = false;
		Map<String, Map<String, Object>> key = new HashMap<>();
		Character curKey = null;
		for (; i < components.length; i++) {
			Object o = components[i];
			if (o instanceof Character) {
				if (curKey != null)
					throw new IllegalArgumentException("Provided two char keys in a row");
				curKey = (Character) o;
			} else {
				if (curKey == null)
					throw new IllegalArgumentException("Providing object without a char key");
				if (o instanceof String)
					isOreDict = true;
				key.put(Character.toString(curKey), serializeItem(o));
				curKey = null;
			}
		}
		json.put("key", key);
		json.put("type", isOreDict ? "forge:ore_shaped" : "minecraft:crafting_shaped");
		json.put("result", serializeItem(result));

		if (group != null) {
			json.put("group", group.toString());
		}

		// names the json the same name as the output's registry name
		// repeatedly adds _alt if a file already exists
		// janky I know but it works
		String suffix = result.getItem().getHasSubtypes() ? "_" + result.getItemDamage() : "";
		File f = new File(RECIPE_DIR, result.getItem().getRegistryName().getResourcePath() + suffix + ".json");

		while (f.exists()) {
			suffix += "_alt";
			f = new File(RECIPE_DIR, result.getItem().getRegistryName().getResourcePath() + suffix + ".json");
		}

		try (FileWriter w = new FileWriter(f)) {
			GSON.toJson(json, w);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void addShapelessRecipe(ItemStack result, Object... components) {
		addShapelessRecipe(result, null, components);
	}
	
	private static void addShapelessRecipe(ItemStack result, ResourceLocation group, Object... components) {
		setupDir();

		// addShapelessRecipe(result, components);

		Map<String, Object> json = new HashMap<>();

		boolean isOreDict = false;
		List<Map<String, Object>> ingredients = new ArrayList<>();
		for (Object o : components) {
			if (o instanceof String)
				isOreDict = true;
			ingredients.add(serializeItem(o));
		}
		json.put("ingredients", ingredients);
		json.put("type", isOreDict ? "forge:ore_shapeless" : "minecraft:crafting_shapeless");
		json.put("result", serializeItem(result));

		if (group != null) {
			json.put("group", group.toString());
		}

		// names the json the same name as the output's registry name
		// repeatedly adds _alt if a file already exists
		// janky I know but it works
		String suffix = result.getItem().getHasSubtypes() ? "_" + result.getItemDamage() : "";
		File f = new File(RECIPE_DIR, result.getItem().getRegistryName().getResourcePath() + suffix + ".json");

		while (f.exists()) {
			suffix += "_alt";
			f = new File(RECIPE_DIR, result.getItem().getRegistryName().getResourcePath() + suffix + ".json");
		}

		try (FileWriter w = new FileWriter(f)) {
			GSON.toJson(json, w);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static Map<String, Object> serializeItem(Object thing) {
		if (thing instanceof Item) {
			return serializeItem(new ItemStack((Item) thing));
		}
		if (thing instanceof Block) {
			return serializeItem(new ItemStack((Block) thing));
		}
		if (thing instanceof ItemStack) {
			ItemStack stack = (ItemStack) thing;
			Map<String, Object> ret = new HashMap<>();
			ret.put("item", stack.getItem().getRegistryName().toString());
			if (stack.getItem().getHasSubtypes() || stack.getItemDamage() != 0) {
				ret.put("data", stack.getItemDamage());
			}
			if (stack.getCount() > 1) {
				ret.put("count", stack.getCount());
			}

			if (stack.hasTagCompound()) {
				ret.put("type", "minecraft:item_nbt");
				ret.put("nbt", stack.getTagCompound().toString());
			}

			return ret;
		}
		if (thing instanceof String) {
			Map<String, Object> ret = new HashMap<>();
			USED_OD_NAMES.add((String) thing);
			ret.put("item", "#" + ((String) thing).toUpperCase(Locale.ROOT));
			return ret;
		}

		throw new IllegalArgumentException("Not a block, item, stack, or od name");
	}

	// Call this after you are done generating
	private static void generateConstants() {
		List<Map<String, Object>> json = new ArrayList<>();
		for (String s : USED_OD_NAMES) {
			Map<String, Object> entry = new HashMap<>();
			entry.put("name", s.toUpperCase(Locale.ROOT));
			entry.put("ingredient", ImmutableMap.of("type", "forge:ore_dict", "ore", s));
			json.add(entry);
		}

		try (FileWriter w = new FileWriter(new File(RECIPE_DIR, "_constants.json"))) {
			GSON.toJson(json, w);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
