package lucraft.mods.lucraftcore.extendedinventory;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

public interface IItemExtendedInventory {

	public ExtendedInventoryItemType getEIItemType(ItemStack stack);

	public default void onWornTick(ItemStack stack, EntityPlayer player) {

	}

	public default void onEquipped(ItemStack itemstack, EntityPlayer player) {

	}

	public default void onUnequipped(ItemStack itemstack, EntityPlayer player) {

	}

	public default void onPressedButton(ItemStack itemstack, EntityPlayer player, boolean pressed) {

	}

	public default boolean canEquip(ItemStack itemstack, EntityPlayer player) {
		return true;
	}

	public default boolean canUnequip(ItemStack itemstack, EntityPlayer player) {
		return true;
	}

	public enum ExtendedInventoryItemType {

		NECKLACE, MANTLE, WRIST;

	}

}
