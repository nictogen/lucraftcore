package lucraft.mods.lucraftcore.superpowers.abilities;

import java.util.List;

import lucraft.mods.lucraftcore.superpowers.keys.AbilityKeys;

public interface IAbilityContainer {

	public List<Ability> getAbilities();
	
	public Ability getAbilityForKey(AbilityKeys key);
	
}
