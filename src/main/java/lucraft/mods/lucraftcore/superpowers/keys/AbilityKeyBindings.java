package lucraft.mods.lucraftcore.superpowers.keys;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.input.Keyboard;

import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.network.LCPacketDispatcher;
import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import lucraft.mods.lucraftcore.superpowers.events.AbilityKeyEvent;
import lucraft.mods.lucraftcore.superpowers.keys.AbilityKeys.AbilityKeyType;
import lucraft.mods.lucraftcore.superpowers.network.MessageAbilityKey;
import lucraft.mods.lucraftcore.superpowers.suitsets.SuitSet;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.client.settings.IKeyConflictContext;
import net.minecraftforge.client.settings.KeyConflictContext;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent.KeyInputEvent;

public class AbilityKeyBindings {

	public static ArrayList<KeyBindingAbility> KEYS = new ArrayList<KeyBindingAbility>();

	public static final KeyBindingAbility KEY_ARMOR_1 = new KeyBindingAbility("lucraftcore.keybinding.keyArmor1", KeyConflictContext.IN_GAME, Keyboard.KEY_H, LucraftCore.NAME, AbilityKeys.ARMOR_1);
	public static final KeyBindingAbility KEY_ARMOR_2 = new KeyBindingAbility("lucraftcore.keybinding.keyArmor2", KeyConflictContext.IN_GAME, Keyboard.KEY_G, LucraftCore.NAME, AbilityKeys.ARMOR_2);
	public static final KeyBindingAbility KEY_ARMOR_3 = new KeyBindingAbility("lucraftcore.keybinding.keyArmor3", KeyConflictContext.IN_GAME, Keyboard.KEY_B, LucraftCore.NAME, AbilityKeys.ARMOR_3);
	public static final KeyBindingAbility KEY_ARMOR_4 = new KeyBindingAbility("lucraftcore.keybinding.keyArmor4", KeyConflictContext.IN_GAME, Keyboard.KEY_N, LucraftCore.NAME, AbilityKeys.ARMOR_4);
	public static final KeyBindingAbility KEY_ARMOR_5 = new KeyBindingAbility("lucraftcore.keybinding.keyArmor5", KeyConflictContext.IN_GAME, Keyboard.KEY_M, LucraftCore.NAME, AbilityKeys.ARMOR_5);

	public static final KeyBindingAbility KEY_SUPERPOWER_1 = new KeyBindingAbility("lucraftcore.keybinding.keySuperpower1", KeyConflictContext.IN_GAME, Keyboard.KEY_Y, LucraftCore.NAME, AbilityKeys.SUPERPOWER_1);
	public static final KeyBindingAbility KEY_SUPERPOWER_2 = new KeyBindingAbility("lucraftcore.keybinding.keySuperpower2", KeyConflictContext.IN_GAME, Keyboard.KEY_P, LucraftCore.NAME, AbilityKeys.SUPERPOWER_2);
	public static final KeyBindingAbility KEY_SUPERPOWER_3 = new KeyBindingAbility("lucraftcore.keybinding.keySuperpower3", KeyConflictContext.IN_GAME, Keyboard.KEY_I, LucraftCore.NAME, AbilityKeys.SUPERPOWER_3);
	public static final KeyBindingAbility KEY_SUPERPOWER_4 = new KeyBindingAbility("lucraftcore.keybinding.keySuperpower4", KeyConflictContext.IN_GAME, Keyboard.KEY_V, LucraftCore.NAME, AbilityKeys.SUPERPOWER_4);
	public static final KeyBindingAbility KEY_SUPERPOWER_5 = new KeyBindingAbility("lucraftcore.keybinding.keySuperpower5", KeyConflictContext.IN_GAME, Keyboard.KEY_X, LucraftCore.NAME, AbilityKeys.SUPERPOWER_5);

	public AbilityKeyBindings() {
		registerKeyBinding(KEY_ARMOR_1);
		registerKeyBinding(KEY_ARMOR_2);
		registerKeyBinding(KEY_ARMOR_3);
		registerKeyBinding(KEY_ARMOR_4);
		registerKeyBinding(KEY_ARMOR_5);
		registerKeyBinding(KEY_SUPERPOWER_1);
		registerKeyBinding(KEY_SUPERPOWER_2);
		registerKeyBinding(KEY_SUPERPOWER_3);
		registerKeyBinding(KEY_SUPERPOWER_4);
		registerKeyBinding(KEY_SUPERPOWER_5);
	}

	public static void registerKeyBinding(KeyBindingAbility key) {
		ClientRegistry.registerKeyBinding(key);
		KEYS.add(key);
	}

	public static KeyBinding getKeyBindingFromKeyType(AbilityKeys key) {
		for (KeyBindingAbility kb : KEYS) {
			if (kb.getKeyType() == key) {
				return kb;
			}
		}

		return null;
	}

	public static AbilityKeys getKeyFromArmorAbility(EntityPlayer player, List<Ability> abilities, SuitSet suitSet, Ability ability) {
		for (AbilityKeys keys : AbilityKeys.values()) {
			if (keys.type == AbilityKeyType.ARMOR_ACTION && ability == suitSet.getSuitAbilityForKey(keys, abilities)) {
				return keys;
			}
		}

		return null;
	}
	
	@SubscribeEvent
	public void onKey(KeyInputEvent evt) {
		for (KeyBindingAbility key : KEYS) {
			if (key.isKeyDown() != key.isPressed) {
				key.isPressed = !key.isPressed;
				MinecraftForge.EVENT_BUS.post(new AbilityKeyEvent.Client(key.getKeyType(), key.isPressed));
			}
		}
	}

	@SubscribeEvent
	public void onLucraftKey(AbilityKeyEvent.Client e) {
		LCPacketDispatcher.sendToServer(new MessageAbilityKey(e.pressed, e.type));
	}

	public static class KeyBindingAbility extends KeyBinding {

		private AbilityKeys type;
		public boolean isPressed;

		public KeyBindingAbility(String description, IKeyConflictContext keyConflictContext, int keyCode, String category, AbilityKeys type) {
			super(description, keyConflictContext, keyCode, category);
			this.type = type;
		}

		public AbilityKeys getKeyType() {
			return type;
		}

		@Override
		public String getDisplayName() {
			return super.getDisplayName();
		}

	}

}
