package lucraft.mods.lucraftcore.superpowers.suitsets;

import java.util.ArrayList;
import java.util.List;

import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import lucraft.mods.lucraftcore.superpowers.keys.AbilityKeyBindings;
import lucraft.mods.lucraftcore.superpowers.keys.AbilityKeys;
import lucraft.mods.lucraftcore.util.helper.PlayerHelper;
import lucraft.mods.lucraftcore.util.helper.StringHelper;
import lucraft.mods.lucraftcore.util.items.ExtendedTooltip.IExtendedItemToolTip;
import lucraft.mods.lucraftcore.util.items.OpenableArmor.IOpenableArmor;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemSuitSetArmor extends ItemArmor implements IOpenableArmor, IExtendedItemToolTip {

	public SuitSet suitSet;
	
	public ItemSuitSetArmor(SuitSet suitSet, EntityEquipmentSlot armorSlot) {
		super(suitSet.getArmorMaterial(armorSlot), 0, armorSlot);
		
		if(suitSet.showInCreativeTab())
			this.setCreativeTab(suitSet.getCreativeTab());
		else
			this.setCreativeTab(null);
		this.suitSet = suitSet;
		this.setUnlocalizedName(suitSet.getUnlocalizedName() + "_" + getArmorSlotName(armorSlot).toLowerCase());
	}
	
	@Override
	public ArmorMaterial getArmorMaterial() {
		return suitSet.getArmorMaterial(armorType);
	}
	
	@Override
	public CreativeTabs getCreativeTab() {
		if(suitSet.showInCreativeTab())
			return suitSet.getCreativeTab();
		else
			return null;
	}
	
	public SuitSet getSuitSet() {
		return suitSet;
	}
	
	@Override
	public void addInformation(ItemStack stack, World worldIn, List<String> tooltip, ITooltipFlag advanced) {
		if(getSuitSet().hasExtraDescription(stack))
			tooltip.addAll(getSuitSet().getExtraDescription(stack));
	}

	@Override
	public String getItemStackDisplayName(ItemStack stack) {
		return getSuitSet().getDisplayNameForItem(this, stack, this.armorType, super.getItemStackDisplayName(stack));
	}
	
	@Override
	public String getArmorTexture(ItemStack stack, Entity entity, EntityEquipmentSlot slot, String type) {
		return getSuitSet().getArmorTexturePath(stack, entity, slot, false, entity instanceof EntityPlayer ? PlayerHelper.hasSmallArms((EntityPlayer)entity) : false, isArmorOpen(entity, stack));
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public ModelBiped getArmorModel(EntityLivingBase entityLiving, ItemStack itemStack, EntityEquipmentSlot armorSlot, ModelBiped _default) {
		ModelBiped armorModel = null;

		if (!itemStack.isEmpty()) {
			boolean smallArms = false;
			
			if(entityLiving instanceof EntityPlayer)
				smallArms = PlayerHelper.hasSmallArms((EntityPlayer) entityLiving);
			
			armorModel = getSuitSet().getArmorModel(itemStack, entityLiving, armorSlot, false, smallArms, isArmorOpen(entityLiving, itemStack));

			if (armorModel != null) {
				armorModel.setModelAttributes(_default);
				return armorModel;
			}
		}

		return super.getArmorModel(entityLiving, itemStack, armorSlot, _default);
	}
	
	public String getArmorSlotName(EntityEquipmentSlot slot) {
		switch (slot) {
		case HEAD:
			return "Helmet";
		case CHEST:
			return "Chestplate";
		case LEGS:
			return "Legs";
		case FEET:
			return "Boots";
		default:
			return "";
		}
	}

	// TODO First Person Armor
//	@Override
//	public HashMap<ModelBiped, Object[]> getFirstPersonModels(ItemStack stack, EntityPlayer player, EntityEquipmentSlot armorSlot, boolean smallArms) {
//		HashMap<ModelBiped, Object[]> map = new HashMap<ModelBiped, Object[]>();
//		map.put(getSuitSet().getArmorModel(stack, player, armorSlot, false, smallArms, false), new Object[] {getSuitSet().getArmorTexturePath(stack, player, armorSlot, false, smallArms, false), false});
//		if(getSuitSet().hasGlowyThings(player, armorType))
//			map.put(getSuitSet().getArmorModel(stack, player, armorSlot, false, smallArms, false), new Object[] {getSuitSet().getArmorTexturePath(stack, player, armorSlot, true, smallArms, false), true});
//		return map;
//	}

	@Override
	public void setArmorOpen(Entity player, ItemStack stack, boolean open) {
		if(getSuitSet().canOpenArmor(this.armorType)) {
			NBTTagCompound nbt = new NBTTagCompound();
			if(stack.hasTagCompound())
				nbt = stack.getTagCompound();
			nbt.setBoolean("IsOpen", !nbt.getBoolean("IsOpen"));
			onArmorToggled(player, stack, nbt.getBoolean("IsOpen"));
			stack.setTagCompound(nbt);
		}
	}

	@Override
	public boolean isArmorOpen(Entity entity, ItemStack stack) {
		if(getSuitSet().canOpenArmor(this.armorType) && stack.hasTagCompound())
			return stack.getTagCompound().getBoolean("IsOpen");
			
		return false;
	}
	
	@Override
	public void onArmorToggled(Entity entity, ItemStack stack, boolean open) {
		getSuitSet().onArmorToggled(entity, stack, this.armorType, open);
	}

	@Override
	public boolean shouldShiftTooltipAppear(ItemStack stack, EntityPlayer player) {
		return false;
	}

	@Override
	public List<String> getShiftToolTip(ItemStack stack, EntityPlayer player) {
		return null;
	}

	@Override
	public boolean shouldCtrlTooltipAppear(ItemStack stack, EntityPlayer player) {
		return true;
	}

	@Override
	public List<String> getCtrlToolTip(ItemStack stack, EntityPlayer player) {
		List<String> list = new ArrayList<>();
		List<Ability> abilities = getSuitSet().getDefaultAbilities(player, new ArrayList<>());

		if (abilities.size() > 0) {
			list.add(TextFormatting.RED + StringHelper.translateToLocal("lucraftcore.info.abilities") + TextFormatting.DARK_RED + ":");
			for (Ability ability : abilities) {
				AbilityKeys key = AbilityKeyBindings.getKeyFromArmorAbility(player, abilities, suitSet, ability);
				list.add(TextFormatting.RED + "- " + TextFormatting.GRAY + ability.getDisplayName() + (key == null ? "" : TextFormatting.DARK_RED + " (" + TextFormatting.RED + GameSettings.getKeyDisplayString(AbilityKeyBindings.getKeyBindingFromKeyType(key).getKeyCode()) + TextFormatting.DARK_RED + ")"));
			}
		}
		return list;
	}
	
}