package lucraft.mods.lucraftcore.superpowers.render;

import lucraft.mods.lucraftcore.superpowers.Superpower;
import lucraft.mods.lucraftcore.superpowers.SuperpowerHandler;
import lucraft.mods.lucraftcore.superpowers.SuperpowerPlayerHandler;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.RenderLivingBase;
import net.minecraft.client.renderer.entity.RenderPlayer;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.MobEffects;
import net.minecraft.util.EnumHandSide;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.event.RenderHandEvent;
import net.minecraftforge.client.event.RenderSpecificHandEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class SuperpowerRenderer implements LayerRenderer<EntityPlayer> {

	public RenderPlayer renderer;
	public static Minecraft mc = Minecraft.getMinecraft();
	private static boolean eventHandlerRegistered = false;
	
	public SuperpowerRenderer(RenderPlayer renderer) {
		this.renderer = renderer;
		
		if(!eventHandlerRegistered) {
			MinecraftForge.EVENT_BUS.register(this);
			eventHandlerRegistered = true;
		}
	}

	@Override
	public void doRenderLayer(EntityPlayer player, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch, float scale) {
		if (player.getActivePotionEffect(MobEffects.INVISIBILITY) != null)
			return;

		Superpower superpower = SuperpowerHandler.getSuperpower(player);
		if(superpower != null && superpower.getPlayerRenderer() != null)
			superpower.getPlayerRenderer().onRenderPlayer(renderer, mc, player, superpower, SuperpowerHandler.getSuperpowerPlayerHandler(player), limbSwing, limbSwingAmount, partialTicks, ageInTicks, netHeadYaw, headPitch, scale);
	}
	
	@SubscribeEvent
	public void onRenderSpecificHand(RenderSpecificHandEvent e) {
		Superpower superpower = SuperpowerHandler.getSuperpower(mc.player);
		if(superpower != null && superpower.getPlayerRenderer() != null)
			superpower.getPlayerRenderer().onRenderSpecificHandEvent(e);
	}
	
	@SubscribeEvent
	public void onRenderHand(RenderHandEvent e) {
		Superpower superpower = SuperpowerHandler.getSuperpower(mc.player);
		if(superpower != null && superpower.getPlayerRenderer() != null)
			superpower.getPlayerRenderer().onRenderHandEvent(e);
	}
	
	@SubscribeEvent
	public void onRenderHUD(RenderGameOverlayEvent e) {
		Superpower superpower = SuperpowerHandler.getSuperpower(mc.player);
		if(superpower != null && superpower.getPlayerRenderer() != null)
			superpower.getPlayerRenderer().onRenderGameOverlay(e);
	}

	@Override
	public boolean shouldCombineTextures() {
		return false;
	}

	public static interface ISuperpowerRenderer {

		@SideOnly(Side.CLIENT)
		public void onRenderPlayer(RenderLivingBase<?> renderer, Minecraft mc, EntityPlayer player, Superpower superpower, SuperpowerPlayerHandler handler, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch, float mcScale);

		public default void applyColor() {
			GlStateManager.color(1, 1, 1);
		}

		public default void onRenderHandEvent(RenderHandEvent e) {
		}

		public default void onRenderSpecificHandEvent(RenderSpecificHandEvent e) {
		}

		public default void onRenderGameOverlay(RenderGameOverlayEvent e) {
		}
		
		/**
		 * Gets called when RenderPlayerAPI is installed
		 * 
		 * @param side
		 */
		public default void onRenderFirstPersonArmRPAPI(EnumHandSide side) {
		}

	}

}
