package lucraft.mods.lucraftcore.superpowers.capabilities;

import lucraft.mods.lucraftcore.superpowers.Superpower;
import lucraft.mods.lucraftcore.superpowers.SuperpowerPlayerHandler;
import lucraft.mods.lucraftcore.superpowers.capabilities.CapabilitySuperpower.SuitSetAbilityHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.gameevent.TickEvent.Phase;

public interface ISuperpowerCapability {
	
	public EntityPlayer getPlayer();
	
	public void setSuperpower(Superpower superpower);
	public void setSuperpower(Superpower superpower, boolean update);
	public Superpower getSuperpower();
	public SuperpowerPlayerHandler getSuperpowerPlayerHandler();
	public SuitSetAbilityHandler getSuitSetAbilityHandler();
	public boolean hasPlayedBefore();
	public void setHasPlayedBefore(boolean played);
	
	public void onUpdate(Phase phase);
	
	public NBTTagCompound writeNBT();
	public void readNBT(NBTTagCompound nbt);
	public void loadSuperpowerHandler();
	public void loadSuitSetHandler();
	public void syncToPlayer();
	public void syncToPlayer(EntityPlayer receiver);
	public void syncToAll();
	
}
