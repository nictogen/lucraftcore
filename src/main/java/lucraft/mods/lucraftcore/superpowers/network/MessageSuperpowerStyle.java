package lucraft.mods.lucraftcore.superpowers.network;

import io.netty.buffer.ByteBuf;
import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.network.AbstractServerMessageHandler;
import lucraft.mods.lucraftcore.superpowers.SuperpowerHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageSuperpowerStyle implements IMessage {

	public NBTTagCompound tag;

	public MessageSuperpowerStyle() {
	}

	public MessageSuperpowerStyle(NBTTagCompound tag) {
		this.tag = tag;
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		this.tag = ByteBufUtils.readTag(buf);
	}

	@Override
	public void toBytes(ByteBuf buf) {
		ByteBufUtils.writeTag(buf, tag);
	}

	public static class Handler extends AbstractServerMessageHandler<MessageSuperpowerStyle> {

		@Override
		public IMessage handleServerMessage(final EntityPlayer player, final MessageSuperpowerStyle message, final MessageContext ctx) {

			LucraftCore.proxy.getThreadFromContext(ctx).addScheduledTask(new Runnable() {

				@Override
				public void run() {
					if (SuperpowerHandler.getSuperpowerPlayerHandler(player) != null) {
						SuperpowerHandler.getSuperpowerPlayerHandler(player).setStyleNBTTag(message.tag);
					}
				}

			});

			return null;
		}

	}

}