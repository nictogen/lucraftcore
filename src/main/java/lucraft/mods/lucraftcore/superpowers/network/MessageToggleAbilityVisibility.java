package lucraft.mods.lucraftcore.superpowers.network;

import io.netty.buffer.ByteBuf;
import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.network.AbstractServerMessageHandler;
import lucraft.mods.lucraftcore.superpowers.SuperpowerHandler;
import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageToggleAbilityVisibility implements IMessage {

	public String ability;

	public MessageToggleAbilityVisibility() {
	}

	public MessageToggleAbilityVisibility(Ability ability) {
		this.ability = Ability.ABILITY_REGISTRY.getKey(ability.getAbilityEntry()).toString();
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		ability = ByteBufUtils.readUTF8String(buf);
	}

	@Override
	public void toBytes(ByteBuf buf) {
		ByteBufUtils.writeUTF8String(buf, ability);
	}

	public static class Handler extends AbstractServerMessageHandler<MessageToggleAbilityVisibility> {

		@Override
		public IMessage handleServerMessage(EntityPlayer player, MessageToggleAbilityVisibility message, MessageContext ctx) {
			LucraftCore.proxy.getThreadFromContext(ctx).addScheduledTask(new Runnable() {

				@Override
				public void run() {
					Ability ability = Ability.getAbilityFromClass(SuperpowerHandler.getSuperpowerPlayerHandler(player).getAbilities(), Ability.ABILITY_REGISTRY.getValue(new ResourceLocation(message.ability)).getAbilityClass());

					if (ability != null) {
						ability.setHidden(!ability.isHidden());
						SuperpowerHandler.syncToPlayer(player);
					}
				}
			});
			return null;
		}

	}

}