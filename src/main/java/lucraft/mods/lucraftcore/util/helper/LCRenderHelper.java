package lucraft.mods.lucraftcore.util.helper;

import java.util.Arrays;
import java.util.List;

import lucraft.mods.lucraftcore.LucraftCore;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.Vec3i;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.relauncher.Side;

@EventBusSubscriber(modid = LucraftCore.MODID, value = Side.CLIENT)
public class LCRenderHelper {

	public static Minecraft mc = Minecraft.getMinecraft();
	public static final ResourceLocation utils = new ResourceLocation(LucraftCore.MODID, "textures/gui/utils.png");
	public static ResourceLocation iconTex = new ResourceLocation(LucraftCore.MODID, "textures/gui/icons.png");
	
	public static float renderTick;
	
	@SubscribeEvent
	public static void renderTick(TickEvent.RenderTickEvent e) {
		renderTick = e.renderTickTime;
	}
	
	public static void drawIcon(Minecraft mc, Gui gui, int x, int y, int row, int column) {
		mc.renderEngine.bindTexture(iconTex);
		gui.drawTexturedModalRect(x, y, column * 16, row * 16, 16, 16);
	}

	public static void drawIcon(Minecraft mc, int x, int y, int row, int column) {
		drawIcon(mc, mc.ingameGUI, x, y, row, column);
	}
	
	public static void drawStringWithOutline(String string, int posX, int posY, int fontColor, int outlineColor) {
		mc.fontRenderer.drawString(string, posX + 1, posY, outlineColor);
		mc.fontRenderer.drawString(string, posX - 1, posY, outlineColor);
		mc.fontRenderer.drawString(string, posX, posY + 1, outlineColor);
		mc.fontRenderer.drawString(string, posX, posY - 1, outlineColor);

		mc.fontRenderer.drawString(string, posX, posY, fontColor);
	}
	
	public static void drawGradientRect(int left, int top, int right, int bottom, int startColor, int endColor) {
		float f = (float) (startColor >> 24 & 255) / 255.0F;
		float f1 = (float) (startColor >> 16 & 255) / 255.0F;
		float f2 = (float) (startColor >> 8 & 255) / 255.0F;
		float f3 = (float) (startColor & 255) / 255.0F;
		float f4 = (float) (endColor >> 24 & 255) / 255.0F;
		float f5 = (float) (endColor >> 16 & 255) / 255.0F;
		float f6 = (float) (endColor >> 8 & 255) / 255.0F;
		float f7 = (float) (endColor & 255) / 255.0F;
		GlStateManager.disableTexture2D();
		GlStateManager.enableBlend();
		GlStateManager.disableAlpha();
		GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0);
		GlStateManager.shadeModel(7425);
		Tessellator tessellator = Tessellator.getInstance();
		BufferBuilder worldrenderer = tessellator.getBuffer();
		worldrenderer.begin(7, DefaultVertexFormats.POSITION_COLOR);
		worldrenderer.pos((double) right, (double) top, 300).color(f1, f2, f3, f).endVertex();
		worldrenderer.pos((double) left, (double) top, 300).color(f1, f2, f3, f).endVertex();
		worldrenderer.pos((double) left, (double) bottom, 300).color(f5, f6, f7, f4).endVertex();
		worldrenderer.pos((double) right, (double) bottom, 300).color(f5, f6, f7, f4).endVertex();
		tessellator.draw();
		GlStateManager.shadeModel(7424);
		GlStateManager.disableBlend();
		GlStateManager.enableAlpha();
		GlStateManager.enableTexture2D();
	}

	public static void drawGradientRect(int left, int top, int right, int bottom, Vec3i color1, float alpha1, Vec3i color2, float alpha2) {
		GlStateManager.disableTexture2D();
		GlStateManager.enableBlend();
		GlStateManager.disableAlpha();
		GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0);
		GlStateManager.shadeModel(7425);
		Tessellator tessellator = Tessellator.getInstance();
		BufferBuilder worldrenderer = tessellator.getBuffer();
		worldrenderer.begin(7, DefaultVertexFormats.POSITION_COLOR);
		worldrenderer.pos((double) right, (double) top, 300).color((float) color1.getX(), (float) color1.getY(), (float) color1.getZ(), alpha1).endVertex();
		worldrenderer.pos((double) left, (double) top, 300).color((float) color1.getX(), (float) color1.getY(), (float) color1.getZ(), alpha1).endVertex();
		worldrenderer.pos((double) left, (double) bottom, 300).color((float) color2.getX(), (float) color2.getY(), (float) color2.getZ(), alpha2).endVertex();
		worldrenderer.pos((double) right, (double) bottom, 300).color((float) color2.getX(), (float) color2.getY(), (float) color2.getZ(), alpha2).endVertex();
		tessellator.draw();
		GlStateManager.shadeModel(7424);
		GlStateManager.disableBlend();
		GlStateManager.enableAlpha();
		GlStateManager.enableTexture2D();
	}

	public static void drawRect(int left, int top, int right, int bottom, float red, float green, float blue, float alpha) {
		if (left < right) {
			int i = left;
			left = right;
			right = i;
		}

		if (top < bottom) {
			int j = top;
			top = bottom;
			bottom = j;
		}

		Tessellator tessellator = Tessellator.getInstance();
		BufferBuilder worldrenderer = tessellator.getBuffer();
		GlStateManager.enableBlend();
		GlStateManager.disableTexture2D();
		GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0);
		GlStateManager.color(red, green, blue, alpha);
		worldrenderer.begin(7, DefaultVertexFormats.POSITION);
		worldrenderer.pos((double) left, (double) bottom, 0.0D).endVertex();
		worldrenderer.pos((double) right, (double) bottom, 0.0D).endVertex();
		worldrenderer.pos((double) right, (double) top, 0.0D).endVertex();
		worldrenderer.pos((double) left, (double) top, 0.0D).endVertex();
		tessellator.draw();
		GlStateManager.enableTexture2D();
		GlStateManager.disableBlend();
	}
	
	public static void drawEnergyBar(Gui gui, IEnergyStorage container, Minecraft mc, int x, int y) {
		mc.renderEngine.bindTexture(utils);
		int progress = (int) (((float)container.getEnergyStored() / (float)container.getMaxEnergyStored()) * 42);
		gui.drawTexturedModalRect(x, y, 0, 0, 14, 42);
		gui.drawTexturedModalRect(x, y + 42 - progress, 14, 42 - progress, 14, progress);
	}
	
	public static void drawEnergyBar(Gui gui, IEnergyStorage container, Minecraft mc, int x, int y, int mouseX, int mouseY) {
		drawEnergyBar(gui, container, mc, x, y);
		
		boolean hovered = mouseX >= x && mouseY >= y && mouseX < x + 14 && mouseY < y + 42;
		
		if(hovered) {
			drawStringList(Arrays.asList(container.getEnergyStored() + "/" + container.getMaxEnergyStored() + " RF"), mouseX + 10, mouseY, true);
		}
	}
	
	public static void drawStringList(List<String> list, int posX, int posY, boolean drawBackground) {
		drawStringList(list, posX, posY, 10, drawBackground);
	}

	public static void drawStringList(List<String> list, int posX, int posY, int differencePerLine, boolean drawBackground) {
		GlStateManager.disableRescaleNormal();
		RenderHelper.disableStandardItemLighting();
		GlStateManager.disableLighting();
		GlStateManager.disableDepth();
		int longestString = 0;

		for (int i = 0; i < list.size(); i++) {
			if (longestString < mc.fontRenderer.getStringWidth(list.get(i))) {
				longestString = mc.fontRenderer.getStringWidth(list.get(i));
			}
		}

		if (drawBackground) {
			int color1 = -267386864;
			int color2 = 1347420415;
			int color3 = (color2 & 16711422) >> 1 | color2 & -16777216;

			drawGradientRect(posX - 3, posY - 3, posX + longestString + 3, posY + (list.size() * differencePerLine) + 3, color1, color1);

			drawGradientRect(posX - 4, posY - 4, posX + longestString + 4, posY - 3, color2, color2);
			drawGradientRect(posX - 4, posY - 3, posX + -3, posY + (list.size() * differencePerLine) + 3, color2, color3);
			drawGradientRect(posX + 3 + longestString, posY - 3, posX + 4 + longestString, posY + (list.size() * differencePerLine) + 3, color2, color3);
			drawGradientRect(posX - 4, posY + (list.size() * differencePerLine) + 3, posX + longestString + 4, posY + (list.size() * differencePerLine) + 4, color3, color3);
		}

		for (int i = 0; i < list.size(); i++) {
			mc.fontRenderer.drawStringWithShadow(list.get(i), posX, posY + (i * differencePerLine), -1);
		}

		GlStateManager.enableLighting();
		GlStateManager.enableDepth();
		RenderHelper.enableStandardItemLighting();
		GlStateManager.enableRescaleNormal();
	}
	
	public static void drawFireBar(Gui gui, float process, Minecraft mc, int x, int y) {
		mc.renderEngine.bindTexture(utils);
		int i = (int) (process * 14);
		gui.drawTexturedModalRect(x + 1, y, 0, 42, 13, 14);
		gui.drawTexturedModalRect(x, y + 14 - i, 13, 42 + 14 - i, 14, i);
	}
	
	public static void renderFire(Minecraft mc, String texture) {
		mc.renderEngine.bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);
		Tessellator tessellator = Tessellator.getInstance();
		BufferBuilder buffer = tessellator.getBuffer();
		TextureAtlasSprite sprite = mc.getTextureMapBlocks().getTextureExtry(texture);
		
		GlStateManager.pushMatrix();
		
		buffer.begin(7, DefaultVertexFormats.POSITION_TEX);
		
		buffer.pos(0, 0, 0).tex(sprite.getMaxU(), sprite.getMinV()).endVertex();
		buffer.pos(1, 0, 0).tex(sprite.getMinU(), sprite.getMinV()).endVertex();
		buffer.pos(1, 1, 0).tex(sprite.getMinU(), sprite.getMaxV()).endVertex();
		buffer.pos(0, 1, 0).tex(sprite.getMaxU(), sprite.getMaxV()).endVertex();
		
		buffer.pos(1, 0, 1).tex(sprite.getMaxU(), sprite.getMinV()).endVertex();
		buffer.pos(1, 0, 0).tex(sprite.getMinU(), sprite.getMinV()).endVertex();
		buffer.pos(1, 1, 0).tex(sprite.getMinU(), sprite.getMaxV()).endVertex();
		buffer.pos(1, 1, 1).tex(sprite.getMaxU(), sprite.getMaxV()).endVertex();
		
		buffer.pos(0, 0, 1).tex(sprite.getMaxU(), sprite.getMinV()).endVertex();
		buffer.pos(1, 0, 1).tex(sprite.getMinU(), sprite.getMinV()).endVertex();
		buffer.pos(1, 1, 1).tex(sprite.getMinU(), sprite.getMaxV()).endVertex();
		buffer.pos(0, 1, 1).tex(sprite.getMaxU(), sprite.getMaxV()).endVertex();
		
		buffer.pos(0, 0, 1).tex(sprite.getMaxU(), sprite.getMinV()).endVertex();
		buffer.pos(0, 0, 0).tex(sprite.getMinU(), sprite.getMinV()).endVertex();
		buffer.pos(0, 1, 0).tex(sprite.getMinU(), sprite.getMaxV()).endVertex();
		buffer.pos(0, 1, 1).tex(sprite.getMaxU(), sprite.getMaxV()).endVertex();
		
		// BOTTOM
		buffer.pos(0, 1, 0).tex(sprite.getMaxU(), sprite.getMinV()).endVertex();
		buffer.pos(1, 1, 0).tex(sprite.getMinU(), sprite.getMinV()).endVertex();
		buffer.pos(1, 1, 1).tex(sprite.getMinU(), sprite.getMaxV()).endVertex();
		buffer.pos(0, 1, 1).tex(sprite.getMaxU(), sprite.getMaxV()).endVertex();

		buffer.pos(0, 1, 0).tex(sprite.getMinU(), sprite.getMinV()).endVertex();
		buffer.pos(1, 1, 0).tex(sprite.getMinU(), sprite.getMaxV()).endVertex();
		buffer.pos(1, 1, 1).tex(sprite.getMaxU(), sprite.getMaxV()).endVertex();
		buffer.pos(0, 1, 1).tex(sprite.getMaxU(), sprite.getMinV()).endVertex();
		
		buffer.pos(0, 1, 0).tex(sprite.getMinU(), sprite.getMaxV()).endVertex();
		buffer.pos(1, 1, 0).tex(sprite.getMaxU(), sprite.getMaxV()).endVertex();
		buffer.pos(1, 1, 1).tex(sprite.getMaxU(), sprite.getMinV()).endVertex();
		buffer.pos(0, 1, 1).tex(sprite.getMinU(), sprite.getMinV()).endVertex();
		
		buffer.pos(0, 1, 0).tex(sprite.getMaxU(), sprite.getMaxV()).endVertex();
		buffer.pos(1, 1, 0).tex(sprite.getMaxU(), sprite.getMinV()).endVertex();
		buffer.pos(1, 1, 1).tex(sprite.getMinU(), sprite.getMinV()).endVertex();
		buffer.pos(0, 1, 1).tex(sprite.getMinU(), sprite.getMaxV()).endVertex();
		
		tessellator.draw();
		
		GlStateManager.popMatrix();
	}

	private static float lastBrightnessX = OpenGlHelper.lastBrightnessX;
	private static float lastBrightnessY = OpenGlHelper.lastBrightnessY;
	
	public static void setLightmapTextureCoords(float x, float y) {
		lastBrightnessX = OpenGlHelper.lastBrightnessX;
		lastBrightnessY = OpenGlHelper.lastBrightnessY;
		OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, x, y);
	}
	
	public static void restoreLightmapTextureCoords() {
		OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, lastBrightnessX, lastBrightnessY);
	}
	
}
