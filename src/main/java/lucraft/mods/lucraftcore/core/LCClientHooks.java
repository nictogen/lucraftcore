package lucraft.mods.lucraftcore.core;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FileUtils;

import lucraft.mods.lucraftcore.addonpacks.AddonPackHandler;
import lucraft.mods.lucraftcore.addonpacks.ModuleAddonPacks;
import lucraft.mods.lucraftcore.addonpacks.resourcepacks.AddonPackResourcePack;
import lucraft.mods.lucraftcore.util.events.RenderModelEvent;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraftforge.common.MinecraftForge;

public class LCClientHooks {

	public static void preRenderCallBack(EntityLivingBase entity) {
		RenderModelEvent ev = new RenderModelEvent(entity);
		MinecraftForge.EVENT_BUS.post(ev);
	}
	
	public static void renderBipedPre(ModelBiped model, Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
		RenderModelEvent.SetRotationAngels ev = new RenderModelEvent.SetRotationAngels(entity, model, f, f1, f2, f3, f4, f5, RenderModelEvent.ModelSetRotationAnglesEventType.PRE);
		MinecraftForge.EVENT_BUS.post(ev);
		
		if(!ev.isCanceled()) {
			model.setRotationAngles(ev.limbSwing, ev.limbSwingAmount, ev.partialTicks, ev.ageInTicks, ev.netHeadYaw, ev.headPitch, ev.getEntity());
		}
	}

	public static void renderBipedPost(ModelBiped model, Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
		RenderModelEvent.SetRotationAngels ev = new RenderModelEvent.SetRotationAngels(entity, model, f, f1, f2, f3, f4, f5, RenderModelEvent.ModelSetRotationAnglesEventType.POST);
		MinecraftForge.EVENT_BUS.post(ev);
	}
	
	public static void insertAddonPackResourcePacks(List resourcePacks) {
		if(!ModuleAddonPacks.INSTANCE.isEnabled())
			return;
		
		if (!AddonPackHandler.ADDON_PACKS_DIR.exists()) {
			AddonPackHandler.ADDON_PACKS_DIR.mkdir();
		}
		
		for(File file : FileUtils.listFiles(AddonPackHandler.ADDON_PACKS_DIR, new String[] { "zip" }, false)) {
			resourcePacks.add(new AddonPackResourcePack(file));
		}
	}
	
}
