package lucraft.mods.lucraftcore.advancedcombat;

import lucraft.mods.lucraftcore.LCConfig;
import lucraft.mods.lucraftcore.advancedcombat.capability.CapabilityAdvancedCombat;
import lucraft.mods.lucraftcore.advancedcombat.capability.IAdvancedCombatCapability;
import lucraft.mods.lucraftcore.advancedcombat.keys.AdvancedCombatKeyBindings;
import lucraft.mods.lucraftcore.advancedcombat.network.MessageSendKeyData;
import lucraft.mods.lucraftcore.advancedcombat.network.MessageSyncAdvancedCombat;
import lucraft.mods.lucraftcore.advancedcombat.network.MessageToggleCombatMode;
import lucraft.mods.lucraftcore.module.Module;
import lucraft.mods.lucraftcore.network.LCPacketDispatcher;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * Created by AFlyingGrayson on 12/10/17
 */
public class ModuleAdvancedCombat extends Module
{
	public static final ModuleAdvancedCombat INSTANCE = new ModuleAdvancedCombat();

	@Override
	public void preInit(FMLPreInitializationEvent event)
	{
		// Capability Registering
		CapabilityManager.INSTANCE.register(IAdvancedCombatCapability.class, new CapabilityAdvancedCombat.Storage(), CapabilityAdvancedCombat.class);

		// Event Handler
		MinecraftForge.EVENT_BUS.register(new CapabilityAdvancedCombat.EventHandler());
		MinecraftForge.EVENT_BUS.register(new AdvancedCombatEventHandler());

		// Network
		LCPacketDispatcher.registerMessage(MessageSyncAdvancedCombat.Handler.class, MessageSyncAdvancedCombat.class, Side.CLIENT, 82);
		LCPacketDispatcher.registerMessage(MessageToggleCombatMode.Handler.class, MessageToggleCombatMode.class, Side.SERVER, 83);
		LCPacketDispatcher.registerMessage(MessageSendKeyData.Handler.class, MessageSendKeyData.class, Side.SERVER, 84);
	}

	@Override
	public void init(FMLInitializationEvent event)
	{

	}

	@Override
	public void postInit(FMLPostInitializationEvent event)
	{

	}

	@SideOnly(Side.CLIENT)
	@Override
	public void preInitClient(FMLPreInitializationEvent event)
	{
		MinecraftForge.EVENT_BUS.register(new AdvancedCombatKeyBindings());
	}

	@Override
	public String getName()
	{
		return "Advanced Combat";
	}

	@Override
	public boolean isEnabled()
	{
		return LCConfig.modules.advanced_combat;
	}
}
