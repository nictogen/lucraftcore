package lucraft.mods.lucraftcore.advancedcombat.capability;

import lucraft.mods.lucraftcore.advancedcombat.styles.CombatStyle;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;

/**
 * Created by AFlyingGrayson on 12/10/17
 */
public interface IAdvancedCombatCapability
{
	NBTTagCompound writeNBT();

	void readNBT(NBTTagCompound nbt);

	boolean isCombatModeEnabled();

	void syncToPlayer();

	void syncToPlayer(EntityPlayer receiver);

	void syncToAll();

	boolean getLeftClick();

	boolean getRightClick();

	boolean getDropKey();

	boolean getInvKey();

	void setLeftClick(boolean leftClick);

	void setRightClick(boolean rightClick);

	void setDropKey(boolean dropKey);

	void setInvKey(boolean invKey);

	EntityPlayer getPlayer();

	CombatStyle getCombatStyle();

	void setCombatStyle(CombatStyle style);

	CombatStyle.Handler getCombatStyleHandler();
}
