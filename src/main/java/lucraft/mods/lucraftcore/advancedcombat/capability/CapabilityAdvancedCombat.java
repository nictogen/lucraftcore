package lucraft.mods.lucraftcore.advancedcombat.capability;

import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.advancedcombat.network.MessageSyncAdvancedCombat;
import lucraft.mods.lucraftcore.advancedcombat.styles.CombatStyle;
import lucraft.mods.lucraftcore.network.LCPacketDispatcher;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.WorldServer;
import net.minecraftforge.client.event.RenderHandEvent;
import net.minecraftforge.client.event.RenderPlayerEvent;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

/**
 * Created by AFlyingGrayson on 12/10/17
 */
public class CapabilityAdvancedCombat implements IAdvancedCombatCapability
{

	//TODO should probably change the null style/null checking for combatmode to a blank style, to prevent accidental NPEs later
	//TODO item slots/switching/equipping when in combat mode
	@CapabilityInject(IAdvancedCombatCapability.class)
	public static final Capability<IAdvancedCombatCapability> ADVANCED_COMBAT_CAP = null;

	public EntityPlayer player;
	public CombatStyle style;
	public CombatStyle.Handler styleHandler;
	private boolean leftClick, rightClick, dropKey, invKey;

	public CapabilityAdvancedCombat(EntityPlayer player)
	{
		this.player = player;
	}

	@Override
	public NBTTagCompound writeNBT()
	{
		NBTTagCompound nbt = new NBTTagCompound();
		nbt.setBoolean("leftClick", leftClick);
		nbt.setBoolean("rightClick", rightClick);
		nbt.setBoolean("dropKey", dropKey);
		nbt.setBoolean("invKey", invKey);
		if (this.style != null)
		{
			nbt.setString("style", style.getRegistryName().toString());
			nbt.setTag("styleData", getCombatStyleHandler().serializeNBT());
		}
		return nbt;
	}

	@Override
	public void readNBT(NBTTagCompound nbt)
	{
		this.style = CombatStyle.COMBAT_STYLE_REGISTRY.getValue(new ResourceLocation(nbt.getString("style")));
		if(this.style != null) this.getCombatStyleHandler().deserializeNBT(nbt.getCompoundTag("styleData"));
		this.leftClick = nbt.getBoolean("leftClick");
		this.rightClick = nbt.getBoolean("rightClick");
		this.dropKey = nbt.getBoolean("dropKey");
		this.invKey = nbt.getBoolean("invKey");
	}

	@Override public boolean isCombatModeEnabled()
	{
		return this.style != null;
	}

	@Override
	public void syncToPlayer()
	{
		this.syncToPlayer(this.player);
	}

	@Override
	public void syncToPlayer(EntityPlayer receiver)
	{
		if (receiver instanceof EntityPlayerMP)
			LCPacketDispatcher.sendTo(new MessageSyncAdvancedCombat(this.player), (EntityPlayerMP) receiver);
	}

	@Override
	public void syncToAll()
	{
		this.syncToPlayer();
		if (player.world instanceof WorldServer)
		{
			for (EntityPlayer players : ((WorldServer) player.world).getEntityTracker().getTrackingPlayers(player))
			{
				if (players instanceof EntityPlayerMP)
				{
					LCPacketDispatcher.sendTo(new MessageSyncAdvancedCombat(player), (EntityPlayerMP) players);
				}
			}
		}
	}

	@Override public boolean getLeftClick()
	{
		return leftClick;
	}

	@Override public boolean getRightClick()
	{
		return rightClick;
	}

	@Override public boolean getDropKey() { return dropKey; }

	@Override public boolean getInvKey() { return invKey; }

	@Override public void setLeftClick(boolean leftClick)
	{
		this.leftClick = leftClick;
	}

	@Override public void setRightClick(boolean rightClick)
	{
		this.rightClick = rightClick;
	}

	@Override public void setDropKey(boolean dropKey) { this.dropKey = dropKey; }

	@Override public void setInvKey(boolean invKey) { this.invKey = invKey; }

	@Override public EntityPlayer getPlayer()
	{
		return this.player;
	}

	@Override public void setCombatStyle(CombatStyle style)
	{
		this.style = style;
	}

	@Override public CombatStyle getCombatStyle()
	{
		return this.style;
	}

	@Override public CombatStyle.Handler getCombatStyleHandler()
	{
		if(this.style != null){
			if(this.styleHandler == null || this.styleHandler.getStyle() != this.style) {
				this.styleHandler = this.style.getHandler(this);
			}
			return this.styleHandler;
		} else return null;
	}

	public static class EventHandler
	{

		@SubscribeEvent
		public void onAttachCapabilities(AttachCapabilitiesEvent<Entity> evt)
		{
			if (!(evt.getObject() instanceof EntityPlayer) || evt.getObject().hasCapability(ADVANCED_COMBAT_CAP, null))
				return;

			evt.addCapability(new ResourceLocation(LucraftCore.MODID, "advanced_combat"),
					new CapabilityAdvancedCombatProvider(new CapabilityAdvancedCombat((EntityPlayer) evt.getObject())));
		}

		@SubscribeEvent
		public void onPlayerStartTracking(PlayerEvent.StartTracking e)
		{
			if (e.getTarget().hasCapability(ADVANCED_COMBAT_CAP, null))
			{
				e.getTarget().getCapability(ADVANCED_COMBAT_CAP, null).syncToPlayer(e.getEntityPlayer());
			}
		}

		@SubscribeEvent
		public void onPlayerClone(PlayerEvent.Clone e)
		{
			NBTTagCompound compound = (NBTTagCompound) ADVANCED_COMBAT_CAP.getStorage()
					.writeNBT(ADVANCED_COMBAT_CAP, e.getOriginal().getCapability(ADVANCED_COMBAT_CAP, null), null);
			ADVANCED_COMBAT_CAP.getStorage().readNBT(ADVANCED_COMBAT_CAP, e.getEntityPlayer().getCapability(ADVANCED_COMBAT_CAP, null), null, compound);
		}

		@SubscribeEvent
		public void onRenderHandEvent(RenderHandEvent event)
		{
			if (Minecraft.getMinecraft().player.hasCapability(ADVANCED_COMBAT_CAP, null))
			{
				IAdvancedCombatCapability capability = Minecraft.getMinecraft().player.getCapability(ADVANCED_COMBAT_CAP, null);
				if (capability.isCombatModeEnabled())
				{
					capability.getCombatStyleHandler().onRenderHand(event);
				}
			}
		}

		@SubscribeEvent
		public void onRenderHandEvent(RenderPlayerEvent.Pre event)
		{
			if (Minecraft.getMinecraft().player.hasCapability(ADVANCED_COMBAT_CAP, null))
			{
				IAdvancedCombatCapability capability = Minecraft.getMinecraft().player.getCapability(ADVANCED_COMBAT_CAP, null);
				if (capability.isCombatModeEnabled())
				{
					capability.getCombatStyleHandler().onRenderPlayer(event);
				}
			}
		}

		@SubscribeEvent
		public void onUpdate(LivingEvent.LivingUpdateEvent event)
		{
			if (event.getEntity() instanceof EntityPlayer && event.getEntity().hasCapability(CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP, null))
			{
				IAdvancedCombatCapability cap = event.getEntity().getCapability(CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP, null);
				if (cap.isCombatModeEnabled())
				{
					cap.getCombatStyleHandler().onUpdate(event);
					cap.getCombatStyleHandler().attackButtonBehavior(cap.getLeftClick(), cap.getRightClick());
					cap.getCombatStyleHandler().invButtonBehavior(cap.getInvKey());
					cap.getCombatStyleHandler().dropButtonBehavior(cap.getDropKey());
				}
			}
		}

	}

	public static class Storage implements Capability.IStorage<IAdvancedCombatCapability>
	{
		@Override
		public NBTBase writeNBT(Capability<IAdvancedCombatCapability> capability, IAdvancedCombatCapability instance, EnumFacing side)
		{
			return instance.writeNBT();
		}

		@Override
		public void readNBT(Capability<IAdvancedCombatCapability> capability, IAdvancedCombatCapability instance, EnumFacing side, NBTBase nbt) {instance.readNBT((NBTTagCompound) nbt);}

	}
}
