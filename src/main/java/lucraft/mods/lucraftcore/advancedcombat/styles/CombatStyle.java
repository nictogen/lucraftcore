package lucraft.mods.lucraftcore.advancedcombat.styles;

import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.advancedcombat.capability.IAdvancedCombatCapability;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.RenderHandEvent;
import net.minecraftforge.client.event.RenderPlayerEvent;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.IForgeRegistryEntry;
import net.minecraftforge.registries.RegistryBuilder;

/**
 * Created by AFlyingGrayson on 1/3/18
 */
@Mod.EventBusSubscriber
public abstract class CombatStyle extends IForgeRegistryEntry.Impl<CombatStyle>
{
	public static IForgeRegistry<CombatStyle> COMBAT_STYLE_REGISTRY;

	@SubscribeEvent
	public static void onRegisterNewRegistries(RegistryEvent.NewRegistry e)
	{
		COMBAT_STYLE_REGISTRY = new RegistryBuilder<CombatStyle>().setName(new ResourceLocation(LucraftCore.MODID, "combat_style")).setType(CombatStyle.class)
				.setIDRange(0, 512).create();
	}

	abstract public Handler getHandler(IAdvancedCombatCapability capability);

	public abstract class Handler implements INBTSerializable<NBTTagCompound>
	{
		protected IAdvancedCombatCapability combatCapability;

		public Handler(IAdvancedCombatCapability combatCapability)
		{
			this.combatCapability = combatCapability;
		}

		abstract public void onRenderHand(RenderHandEvent event);

		abstract public void onRenderPlayer(RenderPlayerEvent.Pre event);

		abstract public void onUpdate(LivingEvent.LivingUpdateEvent event);

		abstract public void attackButtonBehavior(boolean leftButtonDown, boolean rightButtonDown);

		abstract public void dropButtonBehavior(boolean dropButton);

		abstract public void invButtonBehavior(boolean invButton);

		abstract public CombatStyle getStyle();
	}
}
