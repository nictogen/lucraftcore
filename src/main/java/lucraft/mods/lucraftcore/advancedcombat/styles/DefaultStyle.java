package lucraft.mods.lucraftcore.advancedcombat.styles;

import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.advancedcombat.capability.IAdvancedCombatCapability;
import lucraft.mods.lucraftcore.util.helper.PlayerHelper;
import lucraft.mods.lucraftcore.util.render.LimbManipulationUtil;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.RenderLivingBase;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumHandSide;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraftforge.client.event.RenderHandEvent;
import net.minecraftforge.client.event.RenderPlayerEvent;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

import java.util.ArrayList;

/**
 * Created by AFlyingGrayson on 1/3/18
 */
@Mod.EventBusSubscriber
public class DefaultStyle extends CombatStyle
{
	public static DefaultStyle DEFAULT_STYLE = new DefaultStyle();

	public DefaultStyle()
	{
		this.setRegistryName(LucraftCore.MODID, "defaultStyle");
	}

	@Override public CombatStyle.Handler getHandler(IAdvancedCombatCapability capability)
	{
		return new Handler(capability);
	}

	@SubscribeEvent
	public static void onRegisterCombatStyles(RegistryEvent.Register<CombatStyle> event)
	{
		event.getRegistry().register(DEFAULT_STYLE);
	}

	class Handler extends CombatStyle.Handler
	{
		private int mainHandDrawbackTime, offHandDrawbackTime, kickTime;

		public Handler(IAdvancedCombatCapability combatCapability)
		{
			super(combatCapability);
		}

		@Override public void onRenderHand(RenderHandEvent event)
		{
			EntityPlayerSP player = Minecraft.getMinecraft().player;
			RenderLivingBase renderer = (RenderLivingBase) Minecraft.getMinecraft().getRenderManager().getEntityRenderObject(player);
			if (renderer != null && Minecraft.getMinecraft().gameSettings.thirdPersonView == 0)
			{
				ModelBiped model = (ModelBiped) renderer.getMainModel();
				model.setRotationAngles(0, 0, player.ticksExisted, 0f, 0f, 0.0625f, player);

				//Left Arm
				Boolean mainHand = player.getPrimaryHand() == EnumHandSide.LEFT;
				int drawback = mainHand ? mainHandDrawbackTime : offHandDrawbackTime;

				GlStateManager.pushMatrix();
				GlStateManager.translate(-0.1F, -0.28F, -0.55F);
				GlStateManager.rotate(180.0F, 0.0F, 1.0F, 0.0F);
				GlStateManager.rotate(80.0F, 1.0F, 0.0F, 0.0F);
				if (drawback >= 0)
					GlStateManager.translate(0f, drawback / -75f - 0.45F, 0f);
				else
					GlStateManager.translate(0f, -0.25F, 0f);
				Minecraft.getMinecraft().renderEngine.bindTexture(player.getLocationSkin());
				model.bipedLeftArm.rotateAngleX *= 0.3;
				model.bipedLeftArm.rotateAngleY *= 0.3;
				model.bipedLeftArm.rotateAngleZ *= 0.3;
				model.bipedLeftArm.render(0.0625F);
				GlStateManager.popMatrix();

				//Right Arm
				drawback = !mainHand ? mainHandDrawbackTime : offHandDrawbackTime;
				GlStateManager.pushMatrix();
				GlStateManager.translate(0.15F, -0.3F, -0.35F);
				GlStateManager.rotate(185.0F, 0.0F, 1.0F, 0.0F);
				GlStateManager.rotate(80.0F, 1.0F, 0.0F, 0.0F);
				if (drawback >= 0)
					GlStateManager.translate(0f, drawback / -75f - 0.3F, 0f);
				else
					GlStateManager.translate(0f, -0.1F, 0f);
				Minecraft.getMinecraft().renderEngine.bindTexture(player.getLocationSkin());
				model.bipedRightArm.rotateAngleX *= 0.3;
				model.bipedRightArm.rotateAngleY *= 0.3;
				model.bipedRightArm.rotateAngleZ *= 0.3;
				model.bipedRightArm.render(0.0625F);
				GlStateManager.popMatrix();
				event.setCanceled(true);

				//Leg
				if (kickTime > 0)
				{
					float renderKick = (kickTime + event.getPartialTicks() - 1) * 12f;
					GlStateManager.pushMatrix();
					GlStateManager.translate(0.15F, -0.9F, -0.15F);
					GlStateManager.rotate(130.0F, 0.0F, 1.0F, 0.0F);
					GlStateManager.rotate(renderKick, 0.0F, 1.0F, 0.0F);
					GlStateManager.rotate(80.0F, 1.0F, 0.0F, 0.0F);
					Minecraft.getMinecraft().renderEngine.bindTexture(player.getLocationSkin());
					model.bipedLeftLeg.rotateAngleX = 0;
					model.bipedLeftLeg.rotateAngleY = 0;
					model.bipedLeftLeg.rotateAngleZ = 0;
					model.bipedLeftLeg.render(0.0625f);
					GlStateManager.popMatrix();
					event.setCanceled(true);
				}
			}
		}

		@Override public void onRenderPlayer(RenderPlayerEvent.Pre event)
		{
			EntityPlayerSP player = Minecraft.getMinecraft().player;
			RenderLivingBase renderer = (RenderLivingBase) Minecraft.getMinecraft().getRenderManager().getEntityRenderObject(player);
			if (renderer != null && Minecraft.getMinecraft().gameSettings.thirdPersonView != 0)
			{
				float pitch = player.prevRotationPitch + (player.rotationPitch - player.prevRotationPitch) * event.getPartialRenderTick();
				float renderYawOffset = player.prevRenderYawOffset + (player.renderYawOffset - player.prevRenderYawOffset) * event.getPartialRenderTick();
				float yaw = player.prevRotationYawHead + (player.rotationYawHead - player.prevRotationYawHead) * event.getPartialRenderTick();

				Boolean mainHand = player.getPrimaryHand() == EnumHandSide.LEFT;
				int drawback = mainHand ? mainHandDrawbackTime : offHandDrawbackTime;
				LimbManipulationUtil.getLimbManipulator(event.getRenderer(), LimbManipulationUtil.Limb.LEFT_ARM)
						.setAngles(pitch - 100, yaw - renderYawOffset + 10, 0).setOffsets(0f, -drawback / 80f, 0f);
				drawback = !mainHand ? mainHandDrawbackTime : offHandDrawbackTime;
				LimbManipulationUtil.getLimbManipulator(event.getRenderer(), LimbManipulationUtil.Limb.RIGHT_ARM)
						.setAngles(pitch - 100, yaw - renderYawOffset - 10, 0).setOffsets(0f, -drawback / 80f, 0f);

				float renderKick = (kickTime + event.getPartialRenderTick() - 1) * 20f;

				if(kickTime > 0){
					LimbManipulationUtil.getLimbManipulator(event.getRenderer(), LimbManipulationUtil.Limb.LEFT_LEG)
							.setAngles(-50, renderKick - 90, 0);
				}

			}
		}

		@Override public void onUpdate(LivingEvent.LivingUpdateEvent event)
		{

		}

		@Override public void attackButtonBehavior(boolean leftButtonDown, boolean rightButtonDown)
		{
			if (mainHandDrawbackTime >= 0)
			{
				if (leftButtonDown)
					mainHandDrawbackTime += 1;
				else if (mainHandDrawbackTime > 0)
				{
					RayTraceResult rayTrace = PlayerHelper.rayTrace(combatCapability.getPlayer(),
							combatCapability.getPlayer().getEntityAttribute(EntityPlayer.REACH_DISTANCE).getAttributeValue());
					double percent = mainHandDrawbackTime / 30;
					if (rayTrace.entityHit != null)
					{
						rayTrace.entityHit.attackEntityFrom(DamageSource.causePlayerDamage(combatCapability.getPlayer()),
								(float) ((combatCapability.getPlayer().getEntityAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).getAttributeValue() + 2f)
										* percent));
					}
					mainHandDrawbackTime = -10;
				}
				else
					mainHandDrawbackTime = 0;

				if (mainHandDrawbackTime > 30)
					mainHandDrawbackTime = 30;
			}
			else
				mainHandDrawbackTime += 1;

			if (offHandDrawbackTime >= 0)
			{
				if (rightButtonDown)
					offHandDrawbackTime += 1;
				else if (offHandDrawbackTime > 0)
				{
					RayTraceResult rayTrace = PlayerHelper.rayTrace(combatCapability.getPlayer(),
							combatCapability.getPlayer().getEntityAttribute(EntityPlayer.REACH_DISTANCE).getAttributeValue());
					double percent = offHandDrawbackTime / 30;
					if (rayTrace.entityHit != null)
					{
						rayTrace.entityHit.attackEntityFrom(DamageSource.causePlayerDamage(combatCapability.getPlayer()),
								(float) ((combatCapability.getPlayer().getEntityAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).getAttributeValue() + 2f)
										* percent));
					}
					offHandDrawbackTime = -10;
				}
				else
					offHandDrawbackTime = 0;

				if (offHandDrawbackTime > 30)
					offHandDrawbackTime = 30;
			}
			else
				offHandDrawbackTime += 1;
		}

		@Override public void dropButtonBehavior(boolean dropButton)
		{
			if (kickTime == 0)
			{
				if (dropButton && combatCapability.getPlayer().isSneaking())
				{
					kickTime = 1;
				}
			}
			else
			{
				kickTime++;
				if (kickTime == 10)
					kickTime = 0;
				if (kickTime > 0)
				{
					EntityPlayer player = combatCapability.getPlayer();
					if(player.isSneaking())
					{
						double reach = player.getEntityAttribute(EntityPlayer.REACH_DISTANCE).getAttributeValue() - 1f;
						if (kickTime == 5)
						{
							AxisAlignedBB box = player.getEntityBoundingBox().grow(reach, reach, reach);
							ArrayList<EntityLivingBase> list = (ArrayList<EntityLivingBase>) player.world.getEntitiesWithinAABB(EntityLivingBase.class, box);
							for (EntityLivingBase e : list)
							{
								if (e != player && e.getDistance(player) <= reach)
								{
									Vec3d vec3d = e.getPositionVector();
									Vec3d vec3d1 = player.getLook(1.0F);
									Vec3d vec3d2 = vec3d.subtractReverse(new Vec3d(player.posX, player.posY, player.posZ)).normalize();
									vec3d2 = new Vec3d(vec3d2.x, 0.0D, vec3d2.z);
									if (vec3d2.dotProduct(vec3d1) < 0.0D)
									{
										e.attackEntityFrom(DamageSource.causePlayerDamage(player),
												(float) player.getEntityAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).getAttributeValue());
									}
								}
							}

							Vec3d position = player.getPositionVector().add(player.getLookVec().normalize());
							player.world.spawnParticle(EnumParticleTypes.SWEEP_ATTACK, position.x, player.posY + 0.35, position.z, 0, 0, 0);
						}
					} else {
						kickTime = 0;
					}
				}
			}
		}

		@Override public void invButtonBehavior(boolean invButton)
		{

		}

		@Override public CombatStyle getStyle()
		{
			return DEFAULT_STYLE;
		}

		@Override public NBTTagCompound serializeNBT()
		{
			NBTTagCompound nbt = new NBTTagCompound();
			nbt.setInteger("mhDrawbackTime", mainHandDrawbackTime);
			nbt.setInteger("ofDrawbackTime", offHandDrawbackTime);
			nbt.setInteger("kickTime", kickTime);
			return nbt;
		}

		@Override public void deserializeNBT(NBTTagCompound nbtTagCompound)
		{
			this.mainHandDrawbackTime = nbtTagCompound.getInteger("mhDrawbackTime");
			this.offHandDrawbackTime = nbtTagCompound.getInteger("ofDrawbackTime");
			this.kickTime = nbtTagCompound.getInteger("kickTime");
		}
	}
}
