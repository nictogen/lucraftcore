package lucraft.mods.lucraftcore.advancedcombat;

import lucraft.mods.lucraftcore.advancedcombat.capability.CapabilityAdvancedCombat;
import lucraft.mods.lucraftcore.advancedcombat.capability.IAdvancedCombatCapability;
import lucraft.mods.lucraftcore.advancedcombat.network.MessageSendKeyData;
import lucraft.mods.lucraftcore.network.LCPacketDispatcher;
import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.util.EnumActionResult;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.relauncher.ReflectionHelper;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

/**
 * Created by AFlyingGrayson on 12/8/17
 */
public class AdvancedCombatEventHandler
{

	private int keyCodeAttack, keyCodeUse, keyCodeInventory, keyCodeDrop;

	@SubscribeEvent
	public void onInteract(PlayerInteractEvent event)
	{
		IAdvancedCombatCapability cap = event.getEntityPlayer().getCapability(CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP, null);
		if (cap.isCombatModeEnabled())
		{
			if (event.isCancelable())
				event.setCanceled(true);
			event.setCancellationResult(EnumActionResult.FAIL);
		}
	}

	@SubscribeEvent
	public void onUpdate(TickEvent.ClientTickEvent event)
	{
		if (Minecraft.getMinecraft().player != null && Minecraft.getMinecraft().player.hasCapability(CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP, null))
		{
			IAdvancedCombatCapability cap = Minecraft.getMinecraft().player.getCapability(CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP, null);
			if (cap.isCombatModeEnabled())
			{
				ReflectionHelper.setPrivateValue(KeyBinding.class, Minecraft.getMinecraft().gameSettings.keyBindAttack, false, 8);
				ReflectionHelper.setPrivateValue(KeyBinding.class, Minecraft.getMinecraft().gameSettings.keyBindDrop, false, 8);
				ReflectionHelper.setPrivateValue(KeyBinding.class, Minecraft.getMinecraft().gameSettings.keyBindInventory, false, 8);
				ReflectionHelper.setPrivateValue(KeyBinding.class, Minecraft.getMinecraft().gameSettings.keyBindUseItem, false, 8);
				if(Minecraft.getMinecraft().gameSettings.keyBindAttack.getKeyCode() != 0){
					keyCodeAttack = Minecraft.getMinecraft().gameSettings.keyBindAttack.getKeyCode();
					ReflectionHelper.setPrivateValue(KeyBinding.class, Minecraft.getMinecraft().gameSettings.keyBindAttack, 0, 7);
				}

				if(Minecraft.getMinecraft().gameSettings.keyBindUseItem.getKeyCode() != 0){
					keyCodeUse = Minecraft.getMinecraft().gameSettings.keyBindUseItem.getKeyCode();
					ReflectionHelper.setPrivateValue(KeyBinding.class, Minecraft.getMinecraft().gameSettings.keyBindUseItem, 0, 7);
				}

				if(Minecraft.getMinecraft().gameSettings.keyBindInventory.getKeyCode() != 0){
					keyCodeInventory = Minecraft.getMinecraft().gameSettings.keyBindInventory.getKeyCode();
					ReflectionHelper.setPrivateValue(KeyBinding.class, Minecraft.getMinecraft().gameSettings.keyBindInventory, 0, 7);
				}

				if(Minecraft.getMinecraft().gameSettings.keyBindDrop.getKeyCode() != 0){
					keyCodeDrop = Minecraft.getMinecraft().gameSettings.keyBindDrop.getKeyCode();
					ReflectionHelper.setPrivateValue(KeyBinding.class, Minecraft.getMinecraft().gameSettings.keyBindDrop, 0, 7);
				}

				boolean leftClick = (keyCodeAttack > 0) ? Keyboard.isKeyDown(keyCodeAttack) : Mouse.isButtonDown(keyCodeAttack + 100);
				boolean rightClick = (keyCodeUse > 0) ? Keyboard.isKeyDown(keyCodeUse) : Mouse.isButtonDown(keyCodeUse + 100);
				boolean dropKey = (keyCodeDrop > 0) ? Keyboard.isKeyDown(keyCodeDrop) : Mouse.isButtonDown(keyCodeDrop + 100);
				boolean invKey = (keyCodeInventory > 0) ? Keyboard.isKeyDown(keyCodeInventory) : Mouse.isButtonDown(keyCodeInventory + 100);

				if (cap.getLeftClick() != leftClick || cap.getRightClick() != rightClick || cap.getDropKey() != dropKey || cap.getInvKey() != invKey)
				{
					LCPacketDispatcher.sendToServer(new MessageSendKeyData(leftClick, rightClick, dropKey, invKey));
				}
			} else {
				if(keyCodeAttack != 0){
					ReflectionHelper.setPrivateValue(KeyBinding.class, Minecraft.getMinecraft().gameSettings.keyBindAttack, keyCodeAttack, 7);
					keyCodeAttack = 0;
				}

				if(keyCodeUse != 0){
					ReflectionHelper.setPrivateValue(KeyBinding.class, Minecraft.getMinecraft().gameSettings.keyBindUseItem, keyCodeUse, 7);
					keyCodeUse = 0;
				}

				if(keyCodeDrop != 0){
					ReflectionHelper.setPrivateValue(KeyBinding.class, Minecraft.getMinecraft().gameSettings.keyBindDrop, keyCodeDrop, 7);
					keyCodeDrop = 0;
				}

				if(keyCodeInventory != 0){
					ReflectionHelper.setPrivateValue(KeyBinding.class, Minecraft.getMinecraft().gameSettings.keyBindInventory, keyCodeInventory, 7);
					keyCodeInventory = 0;
				}
			}
		}
	}

	@SubscribeEvent
	public void onUpdate(RenderGameOverlayEvent event)
	{
		if (Minecraft.getMinecraft().player != null && Minecraft.getMinecraft().player.hasCapability(CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP, null))
		{
			IAdvancedCombatCapability cap = Minecraft.getMinecraft().player.getCapability(CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP, null);
			if (cap.isCombatModeEnabled() && event.getType() == RenderGameOverlayEvent.ElementType.HOTBAR)
				event.setCanceled(true);
		}
	}

}
