package lucraft.mods.lucraftcore.advancedcombat.network;

import io.netty.buffer.ByteBuf;
import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.advancedcombat.capability.CapabilityAdvancedCombat;
import lucraft.mods.lucraftcore.network.AbstractClientMessageHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

import java.util.UUID;

/**
 * Created by AFlyingGrayson on 12/10/17
 */
public class MessageSyncAdvancedCombat implements IMessage
{
	public UUID playerUUID;
	public NBTTagCompound nbt;

	public MessageSyncAdvancedCombat()
	{
	}

	public MessageSyncAdvancedCombat(EntityPlayer player)
	{
		this.playerUUID = player.getPersistentID();
		nbt = (NBTTagCompound) CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP.getStorage()
				.writeNBT(CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP, player.getCapability(CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP, null), null);
	}

	@Override
	public void fromBytes(ByteBuf buf)
	{
		this.playerUUID = UUID.fromString(ByteBufUtils.readUTF8String(buf));
		this.nbt = ByteBufUtils.readTag(buf);
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		ByteBufUtils.writeUTF8String(buf, this.playerUUID.toString());
		ByteBufUtils.writeTag(buf, this.nbt);
	}

	public static class Handler extends AbstractClientMessageHandler<MessageSyncAdvancedCombat>
	{

		@Override
		public IMessage handleClientMessage(EntityPlayer player, MessageSyncAdvancedCombat message, MessageContext ctx)
		{

			LucraftCore.proxy.getThreadFromContext(ctx).addScheduledTask(() ->
			{
				if (message != null && ctx != null)
				{
					EntityPlayer en = LucraftCore.proxy.getPlayerEntity(ctx).world.getPlayerEntityByUUID(message.playerUUID);

					if (en != null)
					{
						CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP.getStorage()
								.readNBT(CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP, en.getCapability(CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP, null),
										null, message.nbt);
					}
				}
			});

			return null;
		}

	}
}
