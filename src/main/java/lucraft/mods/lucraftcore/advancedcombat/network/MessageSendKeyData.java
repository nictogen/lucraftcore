package lucraft.mods.lucraftcore.advancedcombat.network;

import io.netty.buffer.ByteBuf;
import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.advancedcombat.capability.CapabilityAdvancedCombat;
import lucraft.mods.lucraftcore.advancedcombat.capability.IAdvancedCombatCapability;
import lucraft.mods.lucraftcore.network.AbstractServerMessageHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

/**
 * Created by AFlyingGrayson on 12/11/17
 */
public class MessageSendKeyData implements IMessage
{

	public boolean leftMouse, rightMouse, dropKey, invKey;

	public MessageSendKeyData() {}

	public MessageSendKeyData(boolean leftMouse, boolean rightMouse, boolean dropKey, boolean invKey)
	{
		this.leftMouse = leftMouse;
		this.rightMouse = rightMouse;
		this.dropKey = dropKey;
		this.invKey = invKey;
	}

	@Override
	public void fromBytes(ByteBuf buf)
	{
		this.leftMouse = buf.readBoolean();
		this.rightMouse = buf.readBoolean();
		this.dropKey = buf.readBoolean();
		this.invKey = buf.readBoolean();
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		buf.writeBoolean(this.leftMouse);
		buf.writeBoolean(this.rightMouse);
		buf.writeBoolean(this.dropKey);
		buf.writeBoolean(this.invKey);
	}

	public static class Handler extends AbstractServerMessageHandler<MessageSendKeyData>
	{
		@Override
		public IMessage handleServerMessage(EntityPlayer player, MessageSendKeyData message, MessageContext ctx)
		{

			LucraftCore.proxy.getThreadFromContext(ctx).addScheduledTask(() ->
			{
				IAdvancedCombatCapability cap = player.getCapability(CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP, null);
				cap.setRightClick(message.rightMouse);
				cap.setLeftClick(message.leftMouse);
				cap.setInvKey(message.invKey);
				cap.setDropKey(message.dropKey);
				cap.syncToAll();
			});

			return null;
		}
	}
}
