package lucraft.mods.lucraftcore.advancedcombat.network;

import io.netty.buffer.ByteBuf;
import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.advancedcombat.capability.CapabilityAdvancedCombat;
import lucraft.mods.lucraftcore.advancedcombat.capability.IAdvancedCombatCapability;
import lucraft.mods.lucraftcore.advancedcombat.styles.DefaultStyle;
import lucraft.mods.lucraftcore.network.AbstractServerMessageHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageToggleCombatMode implements IMessage {

	public MessageToggleCombatMode() {}
	
	@Override
	public void fromBytes(ByteBuf buf) {

	}

	@Override
	public void toBytes(ByteBuf buf) {

	}
	
	public static class Handler extends AbstractServerMessageHandler<MessageToggleCombatMode> {

		@Override
		public IMessage handleServerMessage(EntityPlayer player, MessageToggleCombatMode message, MessageContext ctx) {
			
			LucraftCore.proxy.getThreadFromContext(ctx).addScheduledTask(() ->
			{
				IAdvancedCombatCapability cap = player.getCapability(CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP, null);
				if(cap.getCombatStyle() == DefaultStyle.DEFAULT_STYLE){
					cap.setCombatStyle(null);
				} else cap.setCombatStyle(DefaultStyle.DEFAULT_STYLE);
				cap.syncToAll();
			});
			
			return null;
		}
		
	}

}
