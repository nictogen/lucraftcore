package lucraft.mods.lucraftcore.advancedcombat.keys;

import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.advancedcombat.network.MessageToggleCombatMode;
import lucraft.mods.lucraftcore.network.LCPacketDispatcher;
import net.minecraft.client.settings.KeyBinding;
import net.minecraftforge.client.settings.KeyConflictContext;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent.KeyInputEvent;
import org.lwjgl.input.Keyboard;

public class AdvancedCombatKeyBindings
{

	private static final KeyBinding COMBAT_MODE = new KeyBinding("lucraftcore.keybinding.keyCombatMode", KeyConflictContext.IN_GAME, Keyboard.KEY_COMMA, LucraftCore.NAME);

	public AdvancedCombatKeyBindings()
	{
		ClientRegistry.registerKeyBinding(COMBAT_MODE);
	}

	@SubscribeEvent
	public void onKey(KeyInputEvent evt)
	{
		if (COMBAT_MODE.isPressed()) LCPacketDispatcher.sendToServer(new MessageToggleCombatMode());
	}
}
