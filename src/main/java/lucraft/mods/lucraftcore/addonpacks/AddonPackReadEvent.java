package lucraft.mods.lucraftcore.addonpacks;

import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.eventhandler.Event;

public class AddonPackReadEvent extends Event {

	private InputStream stream;
	private ResourceLocation loc;
	private ZipFile zipFile;
	private ZipEntry entry;
	private String directory;
	
	public AddonPackReadEvent(InputStream stream, ResourceLocation loc, ZipFile zipFile, ZipEntry entry, String directory) {
		this.stream = stream;
		this.loc = loc;
		this.zipFile = zipFile;
		this.entry = entry;
		this.directory = directory;
	}
	
	public InputStream getInputStream() {
		return stream;
	}

	public ResourceLocation getResourceLocation() {
		return loc;
	}
	
	public ZipFile getZipFile() {
		return zipFile;
	}
	
	public ZipEntry getZipEntry() {
		return entry;
	}
	
	public String getDirectory() {
		return directory;
	}
	
}
