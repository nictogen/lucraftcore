package lucraft.mods.lucraftcore.addonpacks;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import lucraft.mods.lucraftcore.LucraftCore;
import net.minecraft.util.JsonUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class AddonPackHandler {

	private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();
	public static final File ADDON_PACKS_DIR = new File("addonpacks");
	public static final List<AddonPackInfo> ADDON_PACKS = new ArrayList<>();
	
	public static void load() {
		if (!ADDON_PACKS_DIR.exists()) {
			ADDON_PACKS_DIR.mkdir();
		}

		for (File file : FileUtils.listFiles(ADDON_PACKS_DIR, new String[] { "zip" }, false)) {
			ZipFile zipFile;
			try {
				zipFile = new ZipFile(file);
				String zipName = FilenameUtils.getName(zipFile.getName());
				boolean loaded = true;
				AddonPackInfo info = null;
				LucraftCore.LOGGER.info("The addon-pack '" + zipName + "' is loading...");
				{
					Enumeration<? extends ZipEntry> entries = zipFile.entries();
					
					while (entries.hasMoreElements()) {
						ZipEntry entry = entries.nextElement();
						InputStream stream = zipFile.getInputStream(entry);
						if(entry.getName().equals("addonpack.mcmeta") && !entry.getName().contains("\\"))
							info = readAddonPackInfo(stream, zipFile.getName(), file);
						else {
							String s = FilenameUtils.removeExtension(entry.getName()).replace("assets/", "");
							String[] astring = s.split("/", 3);
							if(!entry.isDirectory() && astring.length >= 3) {
								ResourceLocation loc = new ResourceLocation(astring[0], astring[2]);
								MinecraftForge.EVENT_BUS.post(new AddonPackReadEvent(stream, loc, zipFile, entry, astring[1]));
							}
						}
						stream.close();
					}
					
				}
				
				if(info == null) {
					LucraftCore.LOGGER.error("The addon-pack '" + zipFile.getName() + "' is missing a addonpack.mcmeta file.");
					loaded = false;
				} else {
					ADDON_PACKS.add(info);
					Enumeration<? extends ZipEntry> entries = zipFile.entries();
					
					while (entries.hasMoreElements()) {
						ZipEntry entry = entries.nextElement();
						InputStream stream = zipFile.getInputStream(entry);

						if(entry.getName().equalsIgnoreCase(info.getImageLocation()) && FMLCommonHandler.instance().getSide() == Side.CLIENT) {
							info.image = ImageIO.read(stream);
						}
						
						stream.close();
					}
		
				}
				zipFile.close();
				
				if(loaded)
					LucraftCore.LOGGER.info("The addon-pack '" + zipName + "' has finished loading.");
				else
					LucraftCore.LOGGER.info("The addon-pack '" + zipName + "' couldn't be loaded.");
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}
	
	public static AddonPackInfo readAddonPackInfo(InputStream stream, String zipName, File file) {
		BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(stream, StandardCharsets.UTF_8));
		JsonObject jsonobject = (new JsonParser()).parse(bufferedreader).getAsJsonObject();
        
        String name = "";
        String author = "";
        String desc = "";
        String icon = "";
        
        // Name
        if(JsonUtils.hasField(jsonobject, "name"))
        	name = JsonUtils.getString(jsonobject, "name");
        else {
        	LucraftCore.LOGGER.error("The addonpack.mcmeta of the addon-pack \"" + zipName + "\" is missing \"name\".");
        	return null;
        }
        
        // Author
        author = JsonUtils.hasField(jsonobject, "author") ? JsonUtils.getString(jsonobject, "author") : "";
        
        // Author
        desc = JsonUtils.hasField(jsonobject, "description") ? JsonUtils.getString(jsonobject, "description") : "";
        
        // Icon
        icon = JsonUtils.hasField(jsonobject, "icon") ? JsonUtils.getString(jsonobject, "icon") : "";
        
        return new AddonPackInfo(name, author, desc, icon, file);
	}

	@SideOnly(Side.CLIENT)
	public static void loadImages() {
		for(AddonPackInfo info : ADDON_PACKS) {
			info.registerIcon();
		}
	}
	
}
